module.exports = [
  {
    name: 'default',
    type: 'mysql',
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    timezone: '+07:00',
    migrationsTableName: 'nestjs_migrations',
    migrations: ['dist/src/migrations/*.js'],
    cli: {
      migrationsDir: 'src/migrations',
    },
  },
]
