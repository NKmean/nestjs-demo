interface IAppConfig {
  port: number
}

export default (): IAppConfig => ({
  port: parseInt(process.env.PORT, 10) || 3000,
})
