import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'
import { IsEmailFormat } from '../../common/decorators/is-email-format'

export class LoginDto {
  @ApiProperty()
  @IsEmailFormat()
  email: string

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  password: string
}
