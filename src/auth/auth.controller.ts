import { Body, Controller, Post } from '@nestjs/common'
import { ApiTags } from '@nestjs/swagger'
import { LoginDto } from './dtos/auth.dto'
import { AuthService } from './services/auth.service'

@ApiTags('Auth')
@Controller('Auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  login(@Body() data: LoginDto): Promise<{
    accessToken: string;
  }> {
    return this.authService.login(data)
  }
}
