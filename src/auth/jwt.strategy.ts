import { ExtractJwt, Strategy } from 'passport-jwt'
import { PassportStrategy } from '@nestjs/passport'
import { Injectable, UnauthorizedException } from '@nestjs/common'
import { UserRepository } from '../users/repositories/user.repository'
import { InjectRepository } from '@nestjs/typeorm'
import { ConfigService } from '@nestjs/config'

export interface IAuthPayload {
  id: number
}

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(
    @InjectRepository(UserRepository)
    private userRepo: UserRepository,
    private config: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: config.get<string>('jwt.secret'),
    })
  }

  async validate(payload: IAuthPayload): Promise<unknown> {
    const user = await this.userRepo.findOne(payload.id)
    if (!user) {
      throw new UnauthorizedException('Invalid token')
    }
    return user
  }
}
