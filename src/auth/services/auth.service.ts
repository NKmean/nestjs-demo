import { Injectable, UnauthorizedException } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { UserRepository } from '../../users/repositories/user.repository'
import { LoginDto } from '../dtos/auth.dto'
import { JwtService } from '@nestjs/jwt'

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepo: UserRepository,

    private jwtService: JwtService,
  ) {}

  async login(data: LoginDto): Promise<{
    accessToken: string;
  }> {
    const { email, password } = data
    const user = await this.userRepo.findOne({ email })
    if (!user) {
      throw new UnauthorizedException('User does not exist')
    }
    if (user.password !== password) {
      throw new UnauthorizedException('Invalid password')
    }
    return {
      accessToken: this.jwtService.sign({ id: user.id }),
    }
  }
}
