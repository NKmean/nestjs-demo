import {MigrationInterface, QueryRunner} from 'typeorm'

export class User1624341778869 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        CREATE TABLE \`users\` (
            \`id\` INT(10) NOT NULL AUTO_INCREMENT,
            \`name\` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
            \`email\` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
            \`firstname\` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
            \`lastname\` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
            \`status\` INT(10) NULL DEFAULT '0',
            \`password\` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
            \`created\` DATETIME NULL DEFAULT NULL,
            \`updated\` DATETIME NULL DEFAULT NULL,
            \`validated\` DATETIME NULL DEFAULT NULL,
            \`last_login\` DATETIME NULL DEFAULT NULL,
            \`gid\` INT(10) NULL DEFAULT NULL,
            \`contact\` INT(10) NULL DEFAULT NULL,
            \`setting\` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
            \`remember_token\` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
            \`team\` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
            \`phone\` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
            \`dob\` DATE NULL DEFAULT NULL,
            \`avatar\` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
            PRIMARY KEY (\`id\`) USING BTREE
        )
        COLLATE='utf8_unicode_ci'
        ENGINE=InnoDB
        AUTO_INCREMENT=252
        ;
      `)
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('DROP TABLE `users`')
  }
}
