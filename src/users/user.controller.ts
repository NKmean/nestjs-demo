import { Controller, Get, UseGuards } from '@nestjs/common'
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger'
import { UserService } from './services/user.service'
import { AuthGuard } from '@nestjs/passport'
import { UseUser } from '../common/decorators/use-user'
import { User } from './entities/user.entity'

@ApiTags('User')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@Controller('Users')
export class UserController {
  constructor(private userService: UserService) {}

  @Get()
  getAll(): Promise<User[]> {
    return this.userService.getAll()
  }

  @Get('Info')
  info(@UseUser() user: User): User {
    return user
  }
}
