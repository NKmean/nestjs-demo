import * as mtz from 'moment-timezone'

export const moment = mtz
export const TZ = moment.tz.guess()
