import { IsEmailFormat } from './is-email-format'
import { validate } from 'class-validator'

describe('IsEmailFormat', () => {
  class MockDto {
    @IsEmailFormat() email: string
  }

  let data: MockDto
  beforeAll(() => {
    data = new MockDto()
  })

  it('should be success with - , _ , . and alphanumeric', async () => {
    data.email = 'qq-ww_ee.rr@we.q-r'
    const error = await validate(data)
    expect(error).toEqual([])
  })

  it('should be success with ez form', async () => {
    data.email = 'aa@bb.cc'
    const error = await validate(data)
    expect(error).toEqual([])
  })

  it('should validate error with invalid form', async () => {
    data.email = 'a@b.c'
    const error = await validate(data)
    expect(error).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          constraints: { isEmailFormat: '' },
          property: 'email',
          target: { email: data.email },
          value: data.email,
        }),
      ]),
    )
  })

  it('should validate error with invalid form', async () => {
    data.email = 'a@b.c'
    const error = await validate(data)
    expect(error).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          constraints: { isEmailFormat: '' },
          property: 'email',
          target: { email: data.email },
          value: data.email,
        }),
      ]),
    )
  })
})
