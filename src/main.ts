import { BadRequestException, Logger, ValidationPipe } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { NestFactory } from '@nestjs/core'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import { AppModule } from './app.module'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)

  /** enable cors */
  app.enableCors({
    // origin: [/\.globish\.co.th$/, /\.globish\.dev$/],
    origin: true,
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    preflightContinue: false,
    optionsSuccessStatus: 204,
    credentials: true,
  })

  /** add swagger UI */
  const swagger = new DocumentBuilder()
    .setTitle('Globish Admin API')
    .setVersion('1.0')
    .addBearerAuth()
    .build()
  const document = SwaggerModule.createDocument(app, swagger)
  SwaggerModule.setup('api', app, document)

  /** add validate pipe */
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      exceptionFactory: (errors): BadRequestException => {
        const errorMessages = {}
        errors.map((error) => {
          errorMessages[error?.property] = Object.values(error?.constraints)
            .join('. ')
            .trim()
        })
        return new BadRequestException({
          error: 'Bad Request',
          inputError: errorMessages,
        })
      },
    }),
  )

  const config = app.get(ConfigService)
  const logger = app.get(Logger)
  const PORT = config.get('port')

  await app.listen(PORT, (): void => {
    logger.log(`🚀 Server ready at http://localhost:${PORT}`)
  })
}
bootstrap()
